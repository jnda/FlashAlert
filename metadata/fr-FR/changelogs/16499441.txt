2.2.5
* Mise à jour des dépendances
* Correction sur une erreur liée à la vérification du terminal à plat
* Correction sur une erreur liée à l'utilisation de la caméra