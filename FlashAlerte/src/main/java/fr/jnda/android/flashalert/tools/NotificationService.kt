package fr.jnda.android.flashalert.tools

import android.app.Notification
import android.app.NotificationChannel
import android.app.NotificationManager
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.os.Build
import android.provider.Telephony
import android.service.notification.NotificationListenerService
import android.service.notification.StatusBarNotification
import android.telecom.TelecomManager
import android.util.Log
import androidx.annotation.RequiresApi
import androidx.preference.PreferenceManager
import fr.jnda.android.flashalert.FlashAlert
import fr.jnda.android.flashalert.db.AppDatabase
import fr.jnda.android.flashalert.db.AppEntryRepository
import fr.jnda.android.flashalert.impl.CameraAccess
import fr.jnda.android.flashalert.impl.default
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.MainScope
import kotlinx.coroutines.cancel
import kotlinx.coroutines.launch
import java.util.Timer
import java.util.TimerTask


class NotificationService : NotificationListenerService() {

    private lateinit var mContext: Context
    lateinit var mCameraImpl: CameraAccess//CameraImpl
    private val notificationScope = MainScope()
    private val TAG = "NotificationService"
    private lateinit var preferenceManager: SharedPreferences

    override fun onCreate() {
        super.onCreate()
        mContext = applicationContext
        mCameraImpl = CameraAccess(mContext)
        preferenceManager = PreferenceManager.getDefaultSharedPreferences(mContext)
    }

    override fun onNotificationPosted(sbn: StatusBarNotification) {
        val pack = sbn.packageName
        val notification = sbn.notification

        val importance: Int
        val all = !preferenceManager.getBoolean("event_notification_high_priority", true)

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val channelId = notification.channelId ?: ""
            val channels = getChannels()
            val channel = channels.firstOrNull { channelId == (it?.id ?: null) }
            importance = channel?.importance?: run { NotificationManager.IMPORTANCE_DEFAULT }
        } else if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            importance = when (notification.priority) {
                Notification.PRIORITY_MIN -> NotificationManager.IMPORTANCE_MIN
                Notification.PRIORITY_LOW -> NotificationManager.IMPORTANCE_LOW
                Notification.PRIORITY_HIGH -> NotificationManager.IMPORTANCE_HIGH
                Notification.PRIORITY_MAX -> NotificationManager.IMPORTANCE_MAX
                else -> NotificationManager.IMPORTANCE_DEFAULT
            }
        } else {
            importance = notification.priority
        }

        if(!all && importance < 3){
            Log.d(TAG, "onNotificationPosted: cancel")
            return
        }

        notificationScope.launch(Dispatchers.IO)  {

            val smsPackage = Telephony.Sms.getDefaultSmsPackage(mContext)
            val callerPackage = mContext.getSystemService(TelecomManager::class.java)?.defaultDialerPackage?:"empty"

            if (pack != smsPackage && pack != callerPackage && DeviceController.continueEvent(mContext)) {

                val selectorDao = AppDatabase.INSTANCE?.appSelector()
                if (selectorDao != null) {
                    val appEntry = AppEntryRepository(selectorDao)
                    val item = appEntry.getItemByPackage(pack)
                    if (item != null && item.selected) {
                        if (!FlashAlert.isRunning)
                            FlashAlert.isRunning = mCameraImpl.toggleStroboscope(default)

                        Timer().schedule(object : TimerTask() {
                            override fun run() {
                                if (FlashAlert.isRunning) {
                                    FlashAlert.isRunning = false
                                    mCameraImpl.stopStroboscope()
                                }
                            }
                        }, 1500)
                    }
                }
            }
        }
    }

    override fun onNotificationRemoved(sbn: StatusBarNotification) {

        Log.i("Msg", "Notification Removed ${sbn.packageName}")
        if (isDefaultDialer(mContext,sbn.packageName)){
            mCameraImpl.stopStroboscope()
        }

    }

    private fun isDefaultDialer(context: Context, packageNameToCheck: String = context.packageName): Boolean {
        val dialingIntent = Intent(Intent.ACTION_DIAL)
        val resolveInfoList = packageManager.queryIntentActivities(dialingIntent, 0)
        if (resolveInfoList.size != 1)
            return false
        return packageNameToCheck == resolveInfoList[0].activityInfo.packageName
    }
    @RequiresApi(Build.VERSION_CODES.O)
    private fun getChannels(): List<NotificationChannel> {
        val ranking = currentRanking
        val channelsList = ArrayList<NotificationChannel>()

        for (notification in activeNotifications) {
            val currentRanking = Ranking()
            ranking.getRanking(notification.key, currentRanking)
            channelsList.add(currentRanking.channel)
        }
        return channelsList
    }

}
