package fr.jnda.android.flashalert

import android.app.Application
import fr.jnda.android.flashalert.db.AppDatabase
import fr.jnda.android.flashalert.tools.ThemeSelector


/**
 * @author Jose N
 * @project FlashAlert
 *
 * Création 30/07/18
 *
 *
 */
class FlashAlert: Application() {
    companion object {
        var isRunning = false
    }

    override fun onCreate() {
        super.onCreate()
        ThemeSelector.setTheme(this)
        AppDatabase.getAppDataBase(this)
    }
}